﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialBulletController : MonoBehaviour {
	private PlayerController _pc;

	// Use this for initialization
	void Start () {
		_pc = GameObject.Find("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) {
		GameObject colObj = other.gameObject;

		if(colObj.tag=="Item" || colObj.tag=="FallingItem"){
			ItemController.Item item = colObj.GetComponent<ItemController>().GetItem();

			// メリットがある効果なら発動
			int point = item.point;
			if(point > 0){
				if(item.itemType == ItemController.ItemType.hpItem){
					_pc.recoverHitPoint(point);
				}else if(item.itemType == ItemController.ItemType.spItem){
					_pc.recoverSkillPoint(point);
				}
			} 

			Destroy(colObj);
		}
	}
}
