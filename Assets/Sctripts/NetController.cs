﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetController : MonoBehaviour {
	private PlayerController _pc;

	// Use this for initialization
	void Start () {
		_pc = GameObject.Find("Player").GetComponent<PlayerController>();
		Invoke("selfDestruct", 20.0f); // 20sec後に自壊
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) {
		GameObject colObj = other.gameObject;

		if(colObj.tag=="Item" || colObj.tag=="FallingItem"){
			ItemController.Item item = colObj.GetComponent<ItemController>().GetItem();

			// メリットがある効果なら発動
			int point = item.point;
			if(point > 0){
				if(item.itemType == ItemController.ItemType.hpItem){
					_pc.recoverHitPoint(point);
				}else if(item.itemType == ItemController.ItemType.spItem){
					_pc.recoverSkillPoint(point);
				}
			} 

			Destroy(colObj);
		}
	}

	void selfDestruct(){
		Destroy(this.gameObject);
	}
}
