﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour {
	/* public */
	public float interval;
	public float xRange;
	public float baseY;
	public float yRange;
	public float forceRange;

	/* private */
	ItemController _ic;

	Dictionary<ItemController.ItemName, GameObject> _itemPrefabList = new Dictionary<ItemController.ItemName, GameObject>(){
	};

	// Use this for initialization
	void Start () {
		_ic = new ItemController();

		// ItemListの初期化
		InitItems();

		// Itemの生成
		randomInstantiateItem();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void InitItems(){
		_ic.setItemPointList(ItemController.ItemName.hpNormal, ItemController.ItemType.hpItem, 10);
		_itemPrefabList[ItemController.ItemName.hpNormal] = (GameObject)Resources.Load("Prefabs/Item_hpN");

		_ic.setItemPointList(ItemController.ItemName.hpRare, ItemController.ItemType.hpItem, 30);
		_itemPrefabList[ItemController.ItemName.hpRare] = (GameObject)Resources.Load("Prefabs/Item_hpR");

		_ic.setItemPointList(ItemController.ItemName.hpDamage, ItemController.ItemType.hpItem, -25);
		_itemPrefabList[ItemController.ItemName.hpDamage] = (GameObject)Resources.Load("Prefabs/Item_hpD");

		_ic.setItemPointList(ItemController.ItemName.spNormal, ItemController.ItemType.spItem, 10);
		_itemPrefabList[ItemController.ItemName.spNormal] = (GameObject)Resources.Load("Prefabs/Item_spN");

		_ic.setItemPointList(ItemController.ItemName.spRare, ItemController.ItemType.spItem, 30);
		_itemPrefabList[ItemController.ItemName.spRare] = (GameObject)Resources.Load("Prefabs/Item_spR");

		_ic.setItemPointList(ItemController.ItemName.spDamage, ItemController.ItemType.spItem, -25);
		_itemPrefabList[ItemController.ItemName.spDamage] = (GameObject)Resources.Load("Prefabs/Item_spD");
	}

	// ランダムな時間間隔でランダムにItemを生成
	void randomInstantiateItem(){
		float next = Random.Range(interval - 0.5f, interval + 0.5f);
		Invoke("randomInstantiateItem", next);

		ItemController.ItemName itemName;
		int n = (int)(100*Random.Range(0.0f, 1.0f));
		if(0<= n && n < 20){
			itemName = ItemController.ItemName.hpNormal;
		}else if(20 <= n && n < 35){
			itemName = ItemController.ItemName.hpRare;
		}else if(35 <= n && n < 50){
			itemName = ItemController.ItemName.hpDamage;
		}else if(50 <= n && n < 70){
			itemName = ItemController.ItemName.spNormal;
		}else if(70 <= n && n < 85){
			itemName = ItemController.ItemName.spRare;
		}else{
			itemName = ItemController.ItemName.spDamage;
		}

		float f = Random.Range(-forceRange, forceRange);
		GameObject item = InstantiateItem(itemName);
		item.GetComponent<Rigidbody>().AddForce(new Vector3(f, 0, 0));
	}

	// Itemの生成
	GameObject InstantiateItem(ItemController.ItemName itemName){
		
		GameObject itemPrefab = _itemPrefabList[itemName];

		float randomX = Random.Range(-xRange, xRange);
		float randomY = Random.Range(baseY - yRange, baseY + yRange );
		Vector3 pos = new Vector3(randomX, randomY, 0);
		GameObject item = Instantiate (itemPrefab, pos, Quaternion.identity) as GameObject;

		return item;
	}
}
