﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	public Text gameOver;

	PlayerController _pc;

	// Use this for initialization
	void Start () {
		gameOver.enabled = false;
		_pc = GameObject.Find("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		if(_pc.GetHP() <= 0) {
			showGameOver();
		}
	}

	public void showGameOver(){
		gameOver.enabled = true;
		gameOver.text = "Game Over\n Press 'R' to Retry";
	}
}
