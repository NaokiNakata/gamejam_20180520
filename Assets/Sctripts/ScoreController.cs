﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

	public Text scoreText;
	PlayerController _pc;
	private int _score;

	// Use this for initialization
	void Start () {
		_pc = GameObject.Find("Player").GetComponent<PlayerController>();

		if(_pc != null){
			_score = _pc.GetScore();
		}
	}
	
	// Update is called once per frame
	void Update () {
		_score = _pc.GetScore();
		Debug.Log("Get Score" + _score);
		scoreText.text = "" + _score;
	}
}
