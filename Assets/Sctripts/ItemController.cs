﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour {

	/* public */

	public float m_offsetRange = 2.0f;
	public float m_power = 2.0f;
	public ItemName itemName;

	public enum ItemType{
		hpItem,
		spItem
	}

	// アイテムの種類
	public struct Item{
		public ItemType itemType;
		public int point;
	}

	public enum ItemName{
		hpNormal,
		hpRare,
		hpDamage,
		spNormal,
		spRare,
		spDamage
	}

	static private Dictionary<ItemName, Item> _itemPointList = new Dictionary<ItemName, Item> (){
	};

	/* private */
	private Rigidbody _rb;

	// Use this for initialization
	void Start () {
		_rb = GetComponent<Rigidbody>();

		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision){
		GameObject colObj = collision.gameObject;

		if (colObj.tag == "Player"){
			foreach (ContactPoint point in collision.contacts){
			}
		}
	}

	void OnTriggerEnter(Collider other) {
		GameObject othObj = other.gameObject;

		if(othObj.tag == "Bullet"){
			HitBullet();
		}
	}

	// 弾が当たった時の挙動
	void HitBullet(){
		// 固定を解除
		_rb.constraints = RigidbodyConstraints.None;
		_rb.constraints = RigidbodyConstraints.FreezePositionZ;

		// 重力を有効化
		_rb.useGravity = true;

		// タグを落下中に変更
		this.gameObject.tag = "FallingItem";

		// 吹き飛ばす
        Vector3 randomDir       = ( Random.insideUnitSphere - Vector3.zero ).normalized;
        float   randomLength    = Random.Range( -m_offsetRange, m_offsetRange );
        Vector3 power   = m_power * randomDir;
        Vector3 pos     = transform.position + transform.up * randomLength;
        _rb.AddForceAtPosition( power, pos, ForceMode.Impulse );
    }

	/* Getter */
	public Item GetItem(){
		return _itemPointList[itemName];
	}

	/* Setter */
	public void setItemPointList(ItemName itemName, ItemType itemType, int point){
		_itemPointList[itemName] = new Item{itemType = itemType, point = point};
	}
}
