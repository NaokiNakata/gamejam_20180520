﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBarController : MonoBehaviour {
	Slider _slider;
	PlayerController _pc;

	private float _hp;

	// Use this for initialization
	void Start () {
		_slider = GameObject.Find("HPBar").GetComponent<Slider>();
		_pc = GameObject.Find("Player").GetComponent<PlayerController>();

		if(_pc != null){
			_hp = _pc.GetHPRatio();
		}
	}
	
	// Update is called once per frame
	void Update () {
		_hp = _pc.GetHPRatio();

		// HPゲージに値を設定
    	_slider.value = _hp;
	}
}
