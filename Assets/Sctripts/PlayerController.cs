﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	/* public */
	public float moveSpeed;              // 移動速度
	public float moveForceMultiplier;    // 移動速度の入力に対する追従度
	public int shootInterval; // 弾の発射間隔
	public int shootPoint; // 弾の発射に必要なポイント
	public int slipDamage; // 毎秒受けるダメージ
	public int maxHitPoint; // 体力の上限
	public int maxSkillPoint; // スキルポイントの上限
	public int score;

	public enum Skill{
		ExpandNet,
		SpecialBullet
	}
	private Dictionary<Skill, int> _skillPointList = new Dictionary<Skill, int>() {
		{Skill.ExpandNet, 30},
		{Skill.SpecialBullet, 40},
	};

	/* private */
	private GameObject _bulletPrefab;
	private GameObject _specialBulletPrefab;
	private GameObject _netPrefab;
	private Rigidbody _rb;
	private float _horizontalInput;
	private int _coolTime;

	private int _hitPoint;

	private int _skillPoint;

	// Use this for initialization
	void Start () {
		_rb = GetComponent<Rigidbody>();

		// Prefabの割り当て
		_bulletPrefab = (GameObject)Resources.Load("Prefabs/Bullet");
		_specialBulletPrefab = (GameObject)Resources.Load("Prefabs/SpecialBullet");
		_netPrefab = (GameObject)Resources.Load("Prefabs/Net");

		// ポイントの初期化
		_hitPoint = maxHitPoint;
		_skillPoint = 0;

		decreaseHitPoint();
	}
	
	// Update is called once per frame
	void Update () {
		_horizontalInput = Input.GetAxis("Horizontal");

		if(Input.GetKey(KeyCode.Space) && _coolTime == 0){
			ShootBullet();
		}else if(Input.GetKey(KeyCode.Z)){
			ActivateSkill(Skill.ExpandNet);
		}else if(Input.GetKey(KeyCode.X)){
			ActivateSkill(Skill.SpecialBullet);
		}else if(Input.GetKey(KeyCode.R)){
			SceneManager.LoadScene("Opening");
		}

		if(_hitPoint <= 0){
			Debug.Log("Game Over");
		}
	}

	void FixedUpdate(){
		Move();

		// クールタイムの計算
		if(_coolTime > 0) _coolTime -= 1;
	}

	void OnCollisionEnter(Collision collision){
		GameObject colObj = collision.gameObject;

		if (colObj.tag == "FallingItem"){
			ItemController.Item item = colObj.GetComponent<ItemController>().GetItem();
			ObtainItem(item);
			Destroy(colObj.gameObject);
		}
	}

	// 移動
	void Move(){
		Vector3 moveVector = Vector3.zero;    // 移動速度の入力
		moveVector.x = moveSpeed * _horizontalInput;
		_rb.AddForce(moveForceMultiplier * (moveVector - _rb.velocity));
	}

	// 弾の発射
	void ShootBullet(){
		// 体力が足りない場合は発射不可
		if(_hitPoint < shootPoint) return;

		Vector3 bulletPos = this.transform.position +  new Vector3(0, 1, 0);
		GameObject bullet = Instantiate(_bulletPrefab, bulletPos, Quaternion.identity) as GameObject;
		bullet.GetComponent<Rigidbody>().AddForce(new Vector3(0, 1000, 0));

		_hitPoint -= shootPoint;
		_coolTime += shootInterval;
	}

	// スキルの発動
	void ActivateSkill(Skill skill){
		if(_skillPoint < _skillPointList[skill]) return;

		if(skill == Skill.ExpandNet){
			ExpandNet();
		}else if(skill == Skill.SpecialBullet){
			ShootSpecialBullert();
		}

		_skillPoint -= _skillPointList[skill];
		_coolTime += shootInterval;
	}

	// Skill: Expand Net
	void ExpandNet(){
		Vector3 netPos = this.transform.position +  new Vector3(0, 5, 0);
		GameObject net = Instantiate(_netPrefab, netPos, Quaternion.identity) as GameObject;
	}

	// Skill: SpecialBullet
	void ShootSpecialBullert(){
		Vector3 bulletPos = this.transform.position +  new Vector3(0, 1, 0);
		GameObject specialBullet = Instantiate(_specialBulletPrefab, bulletPos, Quaternion.identity) as GameObject;
		specialBullet.GetComponent<Rigidbody>().AddForce(new Vector3(0, 1300, 0));
	}

	// HP減少(1sec毎に減少)
	void decreaseHitPoint(){
		if(_hitPoint > 0){
			_hitPoint -= slipDamage;
			Invoke("decreaseHitPoint", 1.0f);
		}
	}

	// HP回復
	public void recoverHitPoint(int hitPoint){
		_hitPoint += hitPoint;
		if(_hitPoint >= maxHitPoint){
			_hitPoint = maxHitPoint;
		}
	}

	// SP回復
	public void recoverSkillPoint(int skillPoint){
		_skillPoint += skillPoint;
		if(_skillPoint >= maxSkillPoint){
			_skillPoint = maxSkillPoint;
		}
	}

	// アイテムの取得
	void ObtainItem(ItemController.Item item){
		if(item.itemType == ItemController.ItemType.hpItem){
			recoverHitPoint(item.point);
		}else if(item.itemType == ItemController.ItemType.spItem){
			recoverSkillPoint(item.point);
		}

		ObtainScore(item.point);
	}

	// スコアの取得
	void ObtainScore(int point){
			score += point;
			Debug.Log(score);
	}

	/* Getter */
	public int GetHP(){
		return _hitPoint;
	}
	public float GetHPRatio(){
		return (float)_hitPoint/maxHitPoint;
	}

	public float GetSPRatio(){
		return (float)_skillPoint/maxSkillPoint;
	}

	public int GetScore(){
		return score;
	}


}
