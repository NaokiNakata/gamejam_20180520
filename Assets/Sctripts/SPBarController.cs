﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SPBarController : MonoBehaviour {
	Slider _slider;
	PlayerController _pc;

	private float _sp;

	// Use this for initialization
	void Start () {
		_slider = GameObject.Find("SPBar").GetComponent<Slider>();
		_pc = GameObject.Find("Player").GetComponent<PlayerController>();

		if(_pc != null){
			_sp = _pc.GetSPRatio();
		}
	}
	
	// Update is called once per frame
	void Update () {
		_sp = _pc.GetSPRatio();

		// SPゲージに値を設定
    	_slider.value = _sp;
	}
}
